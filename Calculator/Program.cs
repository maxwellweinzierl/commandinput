﻿using CommandInputLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    internal class Program
    {
        public static List<CommandAction> Commands = new List<CommandAction>
        {
            new CommandAction("add", command =>
            {
                if (command.Args.Count > 1)
                {
                    var total = 0;
                    total = command.Args.AsEnumerable().Select(int.Parse).Aggregate(total, (current, num) => current + num);

                    Console.WriteLine(total);
                }
                else
                {
                    Console.WriteLine("Not enough arguements!");
                }
            }, new List<string> {"addition"}),
            new CommandAction("sub", command =>
            {
                if (command.Args.Count > 1)
                {
                    var total = int.Parse(command.Args[0]);
                    total = command.Args.AsEnumerable().Skip(1).Select(int.Parse).Aggregate(total, (current, num) => current - num);
                    Console.WriteLine(total);
                }
                else
                {
                    Console.WriteLine("Not enough arguements!");
                }
            }, new List<string> {"subtract"}),
            new CommandAction("help", command =>
            {
                Console.WriteLine("Unknown command!");
            }, new List<string> {"?"}, true),
        };

        private static void Main()
        {
            var manager = new CommandManager(Commands);

            Command cmd;
            while ((cmd = CommandInput.ReadCommand(InputSettings.IgnoreCommandCase)) != Command.Empty)
            {
                manager.Run(cmd);
            }
        }
    }
}